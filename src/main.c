#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>

#define SIZE 1024

void print_success(const char* test_name) {
    printf("%s successful.\n", test_name);
}

void run_test(void (*test_function)(), const char* test_name) {
    printf("Running test: %s\n", test_name);
    (*test_function)();
    print_success(test_name);
}

void test_successful_allocation() {
    void* heap = heap_init(SIZE);
    assert(heap);
    void* block = _malloc(200);
    assert(block);
    _free(block);
    heap_term();
}

void test_freeing_one_block() {
    void* heap = heap_init(SIZE);
    assert(heap);
    void* block1 = _malloc(200);
    void* block2 = _malloc(200);
    _free(block1);
    assert(block2);
    _free(block2);
    heap_term();
}

void test_freeing_two_blocks() {
    void* heap = heap_init(SIZE);
    assert(heap);
    void* block1 = _malloc(200);
    void* block2 = _malloc(200);
    void* block3 = _malloc(200);
    _free(block1);
    _free(block2);
    assert(block3);
    heap_term();
}

void test_heap_expansion() {
    void* heap = heap_init(SIZE);
    assert(heap);
    void* block1 = _malloc(512);
    void* block2 = _malloc(512);
    assert(block1);
    assert(block2);
    void* block3 = _malloc(200);
    assert(block3);
    heap_term();
}

void test_heap_expansion_in_other_place() {
    void* heap = heap_init(4*SIZE);
    assert(heap);

    void* hole = mmap(HEAP_START + REGION_MIN_SIZE, 200, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
    assert(hole == HEAP_START + REGION_MIN_SIZE);

    void* new_block = _malloc(REGION_MIN_SIZE - BLOCK_HEADER_SIZE);
    struct block_header* new_block_header =(struct block_header *) (new_block - BLOCK_HEADER_SIZE);

    assert(new_block);
    assert(new_block != hole);
    assert(new_block_header->capacity.bytes >= (REGION_MIN_SIZE - BLOCK_HEADER_SIZE));

    munmap(hole, 200);
    _free(new_block);
    heap_term();
}

int main() {
    void (*test_functions[])() = {
            test_successful_allocation,
            test_freeing_one_block,
            test_freeing_two_blocks,
            test_heap_expansion,
            test_heap_expansion_in_other_place
    };

    const char* test_names[] = {
            "test_successful_allocation",
            "test_freeing_one_block",
            "test_freeing_two_blocks",
            "test_heap_expansion",
            "test_heap_expansion_in_other_place"
    };

    const int num_tests = sizeof(test_functions) / sizeof(test_functions[0]);

    for (int i = 0; i < num_tests; ++i) {
        run_test(test_functions[i], test_names[i]);
    }

    printf("All tests passed successfully.\n");
    return 0;
}
